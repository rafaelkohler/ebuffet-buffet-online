package br.com.ebuffet.eBuffet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ebuffet.eBuffet.entity.Usuario;
import br.com.ebuffet.eBuffet.service.UsuarioService;
import lombok.Data;

@Data
@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping(consumes = "application/json", value = "/criar")
	public Usuario salvar(@RequestBody Usuario usuario) {
		return usuarioService.salvarUsuario(usuario);
	}	

	@GetMapping("/listar")
	public List<Usuario> listar() {
		return usuarioService.listarUsuario();
	}

}
