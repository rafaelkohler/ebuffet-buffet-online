package br.com.ebuffet.eBuffet.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ebuffet.eBuffet.entity.Cliente;
import br.com.ebuffet.eBuffet.entity.Usuario;
import br.com.ebuffet.eBuffet.entity.dto.ClienteUsuarioDTO;
import br.com.ebuffet.eBuffet.service.ClienteService;
import br.com.ebuffet.eBuffet.service.UsuarioService;
import br.com.ebuffet.eBuffet.util.CryptMD5;
import lombok.Data;

@RestController
@Data
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/procurarporid/{id}")
	public Cliente procurarCliente(Long idCliente) {
		return clienteService.procurarClientePorId(idCliente);
	}

	@SuppressWarnings("unused")
	@PostMapping("/criar")
	public String criarUsuario(@Valid @RequestBody ClienteUsuarioDTO clienteUsuario) {

		clienteUsuario.getUsuario().setEmail(clienteUsuario.getUsuario().getEmail().toLowerCase());

		Usuario usu = usuarioService.procurarUsuarioPorLogin(clienteUsuario.getUsuario().getLogin());
		String senha = CryptMD5.encrypt(usu.getLogin(), clienteUsuario.getUsuario().getSenha());

		if (usu == null) {
			usu.setEmail(clienteUsuario.getCliente().getEmail());
			usu.setSenha(senha);
			usu.setTipo(clienteUsuario.getUsuario().getTipo());
			usu.setStatus("A");
			usu.setLogin(clienteUsuario.getUsuario().getLogin());
			usuarioService.salvarUsuario(usu);

			Cliente cli = clienteUsuario.getCliente();
			cli.setUsuario(usu);
			clienteService.salvarCliente(cli);

			return "{\"msg\": \"Cliente criado com sucesso!, \"tipo\": \"sucesso\"}";
		}

		return "{\"msg\": \"Usuário já existe com este login. \", \"tipo\": \"erro\"}";
	}

}
