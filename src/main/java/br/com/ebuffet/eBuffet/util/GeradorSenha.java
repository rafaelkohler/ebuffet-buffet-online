package br.com.ebuffet.eBuffet.util;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GeradorSenha {

    private static final int DIGITOS = 6;

    public static String gerarSenha() {
        String[] keys = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

        List<String> chars = Arrays.asList(keys);
        Random random = new Random();
        String senha = "";
        for (int i = 0; i < DIGITOS; i++) {
            senha += chars.get(random.nextInt(chars.size()));
        }
        return senha;
    }

    public static void main(String[] args) {
        System.out.println(GeradorSenha.gerarSenha());
    }
}
