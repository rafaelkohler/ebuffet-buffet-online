package br.com.ebuffet.eBuffet.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.MaskFormatter;

public class Utils {

    public static final Pattern URL_PATTERN = Pattern.compile("(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)" + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*" + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    public static String dateToStringData(Date data) {
        return dateToString(data, "dd/MM/yyyy");
    }

    public static String dateToStringFull(Date data) {
        return dateToString(data, "dd/MM/yyyy HH:mm:ss");
    }

    public static String formatCurrency(Double value) {
        if (value != null) {
            return NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(value);
        } else {
            return NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(0.0d);
        }
    }

    public static String dateToString(Date data, String pattern) {
        String dataStr = "";
        if (data != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            dataStr = sdf.format(data);
        }
        return dataStr;
    }

    public static Date stringToDate(String data, String pattern) {
        Date dataRet = null;
        if (data != null && !data.isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            try {
                dataRet = sdf.parse(data);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return dataRet;
    }

    public static List<String> parserUrlFromText(String text) {
        List<String> urls = new ArrayList<>();
        Matcher matcher = URL_PATTERN.matcher(text);
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();

            String url = text.substring(matchStart, matchEnd);
            urls.add(url);
        }
        return urls;
    }

    public static String removeUrlFromText(String text) {
        Matcher matcher = URL_PATTERN.matcher(text);
        return matcher.replaceAll("");
    }

    public static String formataPorcentagem(Number valor) {
        if (valor != null) {
            NumberFormat nf = NumberFormat.getNumberInstance();
            nf.setMaximumFractionDigits(2);
            return nf.format(valor);
        } else {
            return "";
        }
    }

    public static String removerMarcara(String campo) {
        if (campo != null && !campo.isEmpty()) {
            campo = campo.replace(".", "");
            campo = campo.replace("/", "");
            campo = campo.replace("-", "");
            campo = campo.replace(" ", "");
        }
        return campo;
    }

    public static String formataCNPJ(String linha) {
        return format("##.###.###/####-##", adicionaZeroEsquerda(linha));
    }

    public static String adicionaZeroEsquerda(String linha) {
        return String.format("%014d", Long.parseLong(linha));
    }

    public static String format(String pattern, Object value) {
        MaskFormatter mask;
        try {
            mask = new MaskFormatter(pattern);
            mask.setValueContainsLiteralCharacters(false);
            return mask.valueToString(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return String.valueOf(value);
    }

    public static String replaceCaracteresEmail(String texto) {
        if (texto != null) {
            texto = texto.replace("\"", "");
            texto = texto.replace("`", "");
            texto = texto.replace("'", "");
            texto = texto.replace("´", "");
        }
        return texto;
    }

    public static boolean isNullOrEmpty(String texto) {
        return texto == null || texto.isEmpty() ? true : false;
    }
}
