package br.com.ebuffet.eBuffet.util;

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Properties;
//import java.util.Set;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//import javax.mail.Authenticator;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.PasswordAuthentication;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.AddressException;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
//
//import org.springframework.core.io.ClassPathResource;
//
//import com.sendgrid.Method;
//import com.sendgrid.Request;
//import com.sendgrid.Response;
//import com.sendgrid.SendGrid;
//import com.sendgrid.helpers.mail.Mail;
//import com.sendgrid.helpers.mail.objects.Content;
//import com.sendgrid.helpers.mail.objects.Email;

public class EnviaEmail {

//    public static final String TEMPLATE_B2C_ANUNCIO_AVISO = "template_b2c_anuncioaviso";
//
//    public static final String TEMPLATE_B2C_CADASTRO = "template_b2c_cadastro";
//
//    public static final String TEMPLATE_B2C_PEDIDO = "template_b2c_pedido";
//
//    public static final String TEMPLATE_B2C_CANCELAMENTO = "template_b2c_cancelamento";
//
//    public static final String TEMPLATE_B2C_NOVA_SENHA = "template_b2c_novasenha";
//
//    public static final String SMTP = "smtp.office365.com";
//
//    public static final String SMTP_PORT = "587";
//
//    public static final String USER = "noreply@chameovendedor.com.br";
//
//    public static final String PASS = "Lum3@321";
//
//    public static final String SEND_GRID_KEY = "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";
//
//    public void enviarEmailPedido(String logo, String para, String numeroPedido, String cliente, String empresa, String valorBruto, String desconto, String valorTotal, String itens) {
//        ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
//        emailExecutor.execute(() -> {
//            Map<String, String> valores = new HashMap<>();
//            valores.put("#LOGOEMPRESA", logo);
//            valores.put("#PEDIDO", numeroPedido);
//            valores.put("#CLIENTE", cliente);
//            valores.put("#EMPRESA", empresa);
//            valores.put("#VALORBRUTO", valorBruto);
//            valores.put("#DESCONTO", desconto);
//            valores.put("#VALORTOTAL", valorTotal);
//            valores.put("#ITENS", itens);
//            String corpo = buscarTemplate(valores, EnviaEmail.TEMPLATE_B2C_PEDIDO);
//            try {
//                enviaEmail(para, "[ChameOVendedor] Recebemos seu pedido!", corpo);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        emailExecutor.shutdown();
//    }
//
//    public void enviarEmailCancelamento(String logo, String para, String cliente, String empresa, String item) {
//        ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
//        emailExecutor.execute(() -> {
//            Map<String, String> valores = new HashMap<>();
//            valores.put("#LOGOEMPRESA", logo);
//            valores.put("#CLIENTE", cliente);
//            valores.put("#EMPRESA", empresa);
//            valores.put("#ITEM", item);
//            String corpo = buscarTemplate(valores, EnviaEmail.TEMPLATE_B2C_CANCELAMENTO);
//            try {
//                enviaEmail(para, "[ChameOVendedor] Recebemos seu cancelamento!", corpo);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        emailExecutor.shutdown();
//    }
//
//    public void enviarEmailCadastro(String logo, String para, String cliente, String empresa, String senhaGerada) {
//        ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
//        emailExecutor.execute(() -> {
//            Map<String, String> valores = new HashMap<>();
//            valores.put("#LOGOEMPRESA", logo);
//            valores.put("#CLIENTE", cliente);
//            valores.put("#EMPRESA", empresa);
//            valores.put("#LOGIN", para);
//            valores.put("#SENHA", senhaGerada);
//            String corpo = buscarTemplate(valores, EnviaEmail.TEMPLATE_B2C_CADASTRO);
//            try {
//                enviaEmail(para, "[ChameOVendedor] Seus dados de acesso!", corpo);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        emailExecutor.shutdown();
//    }
//
//    public void enviarEmailAnuncioAviso(String logo, String para, String cliente, String empresa, String item) {
//        ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
//        emailExecutor.execute(() -> {
//            Map<String, String> valores = new HashMap<>();
//            valores.put("#LOGOEMPRESA", logo);
//            valores.put("#CLIENTE", cliente);
//            valores.put("#EMPRESA", empresa);
//            valores.put("#ITEM", item);
//            String corpo = buscarTemplate(valores, EnviaEmail.TEMPLATE_B2C_ANUNCIO_AVISO);
//            try {
//                enviaEmail(para, "[ChameOVendedor] O item que você estava esperando chegou!", corpo);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        emailExecutor.shutdown();
//    }
//
//    public void enviarEmailNovaSenha(String logo, String para, String cliente, String senha) {
//        ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
//        emailExecutor.execute(() -> {
//            Map<String, String> valores = new HashMap<>();
//            valores.put("#LOGOEMPRESA", logo);
//            valores.put("#CLIENTE", cliente);
//
//            valores.put("#SENHA", senha);
//            String corpo = buscarTemplate(valores, EnviaEmail.TEMPLATE_B2C_NOVA_SENHA);
//            try {
//                enviaEmail(para, "[ChameOVendedor] Sua nova senha de acesso!", corpo);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        emailExecutor.shutdown();
//    }
//
//    public static void main(String[] args) {
//        EnviaEmail e = new EnviaEmail();
//        try {
//            e.enviaEmail("farukz@lumetec.com.br", "teste", "teste");
//        } catch (AddressException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        } catch (MessagingException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        } catch (Exception e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
//    }
//
//    public void enviaEmail(String to_, String subject, String content_) throws AddressException, MessagingException, Exception {
//        Email from = new Email(USER);
//        Email to = new Email(to_);
//        Content content = new Content("text/html", content_);
//        Mail mail = new Mail(from, subject, to, content);
//
//        SendGrid sg = new SendGrid(SEND_GRID_KEY);
//        Request request = new Request();
//        try {
//            request.setMethod(Method.POST);
//            request.setEndpoint("mail/send");
//            request.setBody(mail.build());
//            Response response = sg.api(request);
////            System.out.println(response.getStatusCode());
////            System.out.println(response.getBody());
////            System.out.println(response.getHeaders());
//        } catch (IOException ex) {
//            throw ex;
//        }
//    }
//
//    public void enviaEmail2(String to, String subject, Object content) throws AddressException, MessagingException, Exception {
//        Properties props = new Properties();
//
//        props.put("mail.smtp.host", SMTP);
//        props.put("mail.smtp.port", SMTP_PORT);
//        props.put("mail.smtp.auth", true);
//        props.put("mail.smtp.starttls.enable", true);
//
//        //Session session = Session.getInstance(props);
//        Session session = Session.getInstance(props, new Authenticator() {
//
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(USER, PASS);
//            }
//
//        });
//
//        Message msg = new MimeMessage(session);
//        msg.setFrom(new InternetAddress(USER, "Chame o Vendedor"));
//        if (to.contains(";")) {
//            to = Utils.replaceCaracteresEmail(to);
//            InternetAddress[] intAddress = Arrays.stream(to.split(";")).map(s -> stringToInternetAdress(s)).filter(s -> s != null).toArray(InternetAddress[]::new);
//            msg.addRecipients(Message.RecipientType.BCC, intAddress);
//        } else {
//            msg.setRecipient(Message.RecipientType.BCC, new InternetAddress(to));
//        }
//        msg.setSubject(subject);
//        msg.setContent(content, "text/html; charset=utf-8");
//
//        Transport.send(msg);
//
//    }
//
//    private static InternetAddress stringToInternetAdress(String email) {
//        try {
//            if (!Utils.isNullOrEmpty(email)) {
//                return new InternetAddress(email);
//            } else if (!Utils.isNullOrEmpty(email)) {
//                return new InternetAddress(email);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public String buscarTemplate(Map<String, String> valores, String arquivoTemplate) {
//        try {
//            String conteudo = "";
//            if (arquivoTemplate != null) {
//                StringBuilder sb = new StringBuilder();
//
//                BufferedReader bf = new BufferedReader(new InputStreamReader(new ClassPathResource("templates/" + arquivoTemplate).getInputStream()));
//                while (bf.ready()) {
//                    sb.append(bf.readLine());
//                }
//                conteudo = sb.toString();
//                bf.close();
//            }
//            if (valores != null) {
//                Set<String> chaves = valores.keySet();
//                for (String c : chaves) {
//                    String valor = valores.get(c);
//                    if (valor != null && !valor.isEmpty()) {
//                        conteudo = conteudo.replace(c, valor);
//                    } else {
//                        conteudo = conteudo.replace(c, "");
//                    }
//                }
//            }
//            return conteudo;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return "";
//    }

}
