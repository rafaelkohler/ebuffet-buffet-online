package br.com.ebuffet.eBuffet.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileSystems;
import java.nio.file.StandardOpenOption;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Storage {

    private static final String AZURE_STORAGE_ACCOUNT = "stgisvendas";

    private static final String AZURE_STORAGE_ACCESS_KEY = "95NRnajVpyzEVDQ7xEdGrOgJHSsykTM8qdG88av0oWA9nyD0C7Ww2AojRrk4xDpeuzS+/UfWBCOkYUW5cjERyQ==";

    public static final String AZURE_PATH_IMAGEM_POST = "/imagem-post";

    public static final String AZURE_PATH_IMAGEM_USUARIOS = "/imagem-usuario";

    public static final String AZURE_PATH_IMAGEM_EMPRESAS = "/imagem-empresa";

    public static final String AZURE_LINK_BASE = "https://" + AZURE_STORAGE_ACCOUNT + ".blob.core.windows.net";

    public static final String PATH_SEM_IMAGEM_PECA = "/imagem-peca/sem-imagem.jpg";

    public static final String PATH_SEM_IMAGEM_AVATAR = "/imagem-usuarios/sem-foto.png";

    public static final String PATH_USUARIO_SEM_IMAGEM = "/imagem-usuario/sem-imagem.jpg";

    private static Storage instance;

    private Storage() {
    }

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }

//    public String gravar(InputStream inputStream, String path, String nomeArq) throws Exception {
//        SharedKeyCredentials creds = new SharedKeyCredentials(AZURE_STORAGE_ACCOUNT, AZURE_STORAGE_ACCESS_KEY);
//        URL url = new URL(AZURE_LINK_BASE + path + "/" + nomeArq);
//        final BlockBlobURL blobURL = new BlockBlobURL(url, StorageURL.createPipeline(creds, new PipelineOptions()));
//        AsynchronousFileChannel fileChannel = convertInputStreamToChannel(inputStream);
//        TransferManager.uploadFileToBlockBlob(fileChannel, blobURL, 1, null).blockingGet();
//        fileChannel.close();
//        inputStream.close();
//        return path + "/" + nomeArq;
//    }

    public AsynchronousFileChannel convertInputStreamToChannel(InputStream inputStream) throws IOException, InterruptedException, ExecutionException {
        ReadableByteChannel inputChannel = Channels.newChannel(inputStream);
        AsynchronousFileChannel outputChannel = AsynchronousFileChannel.open(FileSystems.getDefault().getPath("./resources-" + UUID.randomUUID()), StandardOpenOption.CREATE, StandardOpenOption.WRITE,
                StandardOpenOption.READ, StandardOpenOption.DELETE_ON_CLOSE);
        outputChannel.lock();
        final ByteBuffer buffer = ByteBuffer.allocate(4096);
        int position = 0;
        int recievedBytes = 0;
        Future<Integer> lastWrite = null;
        while ((recievedBytes = inputChannel.read(buffer)) >= 0 || buffer.position() != 0) {
            buffer.flip();
            lastWrite = outputChannel.write(buffer, position);
            position += recievedBytes;
            if (lastWrite != null) {
                lastWrite.get();
            }
            buffer.compact();
        }
        inputStream.close();
        inputChannel.close();
        return outputChannel;
    }

}
