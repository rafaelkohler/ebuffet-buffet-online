package br.com.ebuffet.eBuffet.util;

import org.springframework.security.crypto.password.PasswordEncoder;

public class CryptMD5 implements PasswordEncoder {

    public static String encrypt(String user, String password) {
        String sign = user + password;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            md.update(sign.getBytes());
            byte[] hash = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                if ((0xff & hash[i]) < 0x10) {
                    hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
                } else {
                    hexString.append(Integer.toHexString(0xFF & hash[i]));
                }
            }
            sign = hexString.toString();
        } catch (Exception nsae) {
            nsae.printStackTrace();
        }
        return sign;
    }

    public static void main(String[] args) {
        System.out.println(CryptMD5.encrypt("LMACHADO", "SENHA02"));
    }

    @Override
    public String encode(CharSequence rawPassword) {
        try {
            String senha = rawPassword.toString().contains(":") ? rawPassword.toString().split(":")[0] + rawPassword.toString().split(":")[2] : rawPassword.toString();
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            md.update(senha.getBytes());
            byte[] hash = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                if ((0xff & hash[i]) < 0x10) {
                    hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
                } else {
                    hexString.append(Integer.toHexString(0xFF & hash[i]));
                }
            }
            return hexString.toString();
        } catch (Exception nsae) {
            nsae.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encode(rawPassword).equals(encodedPassword);
    }
}
