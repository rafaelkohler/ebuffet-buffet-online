package br.com.ebuffet.eBuffet.entity.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.ebuffet.eBuffet.entity.Cliente;
import br.com.ebuffet.eBuffet.entity.Usuario;
import lombok.Data;

@Data
@Entity
public class ClienteUsuarioDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    @Id
    @ManyToOne
    @JoinColumn(name = "id_cliente")
	private Cliente cliente;
    
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

}
