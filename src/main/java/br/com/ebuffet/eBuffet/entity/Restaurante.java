package br.com.ebuffet.eBuffet.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "restaurantes")
public class Restaurante implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_restaurante")
	private Long idRestaurante;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_localidade")
	private Localidade localidade;

	@Column(name = "razao_social")
	private String razaoSocial;

	@Column(name = "nome_fantasia")
	private String nomeFantasia;

	@Column(name = "documento")
	private String documento;

	@Column(name = "email")
	private String email;

	@Column(name = "celular")
	private String celular;

	@Column(name = "telefone")
	private String telefone;

	@Column(name = "site")
	private String site;

	@Column(name = "horario_atendimento")
	private String horarioAntendimento;

	@Column(name = "valor_quilograma")
	private Double valorQuilograma;

	@OneToMany(mappedBy = "restaurante")
	private List<Produto> produtos;

	@OneToMany(mappedBy = "restaurante")
	@OrderBy("dataPedido desc")
	private List<Pedido> pedidos;

	public Restaurante() {

	}

	public Restaurante(String razaoSocial, String nomeFantasia, String documento, String email, String celular,
			String telefone, String site, String horarioAntendimento, Double valorQuilograma, List<Pedido> pedidos) {
		super();
		this.razaoSocial = razaoSocial;
		this.nomeFantasia = nomeFantasia;
		this.documento = documento;
		this.email = email;
		this.celular = celular;
		this.telefone = telefone;
		this.site = site;
		this.horarioAntendimento = horarioAntendimento;
		this.valorQuilograma = valorQuilograma;
		this.pedidos = pedidos;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Restaurante other = (Restaurante) obj;
		if (idRestaurante == null) {
			if (other.idRestaurante != null)
				return false;
		} else if (!idRestaurante.equals(other.idRestaurante))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idRestaurante == null) ? 0 : idRestaurante.hashCode());
		return result;
	}

}
