package br.com.ebuffet.eBuffet.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "pedido_itens")
public class PedidoItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_pedido_item")
	private Long idPedidoItem;
	
	@ManyToOne
	@JoinColumn(name = "id_cliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "id_produto")
	private Produto produto;

	@ManyToOne
	@JoinColumn(name = "id_restaurante")
	private Restaurante restaurante;

	@ManyToOne
	@JoinColumn(name = "id_pedido")
	private Pedido pedido;
	
	@Column(name = "quantidade")
	private Double quantidade;

	@Column(name = "peso")
	private Double peso;
	
	@Column(name = "unidade_medida")
	private String unidadeMedida;
	
	public PedidoItem() {
		
	}

	public PedidoItem(Double quantidade, Double peso, String unidadeMedida) {
		super();
		this.quantidade = quantidade;
		this.peso = peso;
		this.unidadeMedida = unidadeMedida;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PedidoItem other = (PedidoItem) obj;
		if (idPedidoItem == null) {
			if (other.idPedidoItem != null)
				return false;
		} else if (!idPedidoItem.equals(other.idPedidoItem))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPedidoItem == null) ? 0 : idPedidoItem.hashCode());
		return result;
	}
	
}
