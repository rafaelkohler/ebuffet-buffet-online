package br.com.ebuffet.eBuffet.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name = "pedidos")
public class Pedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_pedido")
	private Long idPedido;

	@ManyToOne
	@JoinColumn(name = "id_cliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "id_localidade")
	private Localidade localidade;

	@ManyToOne
	@JoinColumn(name = "id_restaurante")
	private Restaurante restaurante;

	@ManyToOne
	@JoinColumn(name = "id_frete")
	private Frete frete;

	@Column(name = "valor_bruto")
	private Double valorBruto;

	@Column(name = "valor_liquido")
	private Double valorLiquido;

	@Column(name = "valor_desconto")
	private Double valorDesconto;

	@Column(name = "valor_frete")
	private Double valorFrete;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_pedido")
	private Date dataPedido;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_pag_pedido")
	private Date dataPagPedido;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_entrega")
	private Date dataEntrega;

	@Column(name = "status_pedido")
	private String statusPedido;

	@Column(name = "valor_plataforma")
	private Double valorPlataforma;

	@Column(name = "tipo_pagamento")
	private String tipoPagamento;
	
	@OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PedidoItem> pedidoItens;

	public Pedido(Double valorBruto, Double valorLiquido, Double valorDesconto, Double valorFrete, Date dataPedido,
			Date dataPagPedido, String statusPedido, Double valorPlataforma, String tipoPagamento) {
		super();
		this.valorBruto = valorBruto;
		this.valorLiquido = valorLiquido;
		this.valorDesconto = valorDesconto;
		this.valorFrete = valorFrete;
		this.dataPedido = dataPedido;
		this.dataPagPedido = dataPagPedido;
		this.statusPedido = statusPedido;
		this.valorPlataforma = valorPlataforma;
		this.tipoPagamento = tipoPagamento;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (idPedido == null) {
			if (other.idPedido != null)
				return false;
		} else if (!idPedido.equals(other.idPedido))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPedido == null) ? 0 : idPedido.hashCode());
		return result;
	}
	
	
}
