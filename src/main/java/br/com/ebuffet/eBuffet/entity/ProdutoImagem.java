package br.com.ebuffet.eBuffet.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "produto_imagem")
public class ProdutoImagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_produto_imagem")
	private Long idProdutoImagem; 
	
	@ManyToOne
	@JoinColumn(name = "id_produto")
	private Produto produto;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "path")
	private String path;
	
	public ProdutoImagem() {
		
	}

	public ProdutoImagem(Produto produto, String nome, String path) {
		super();
		this.produto = produto;
		this.nome = nome;
		this.path = path;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoImagem other = (ProdutoImagem) obj;
		if (idProdutoImagem == null) {
			if (other.idProdutoImagem != null)
				return false;
		} else if (!idProdutoImagem.equals(other.idProdutoImagem))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idProdutoImagem == null) ? 0 : idProdutoImagem.hashCode());
		return result;
	}
	
	

}
