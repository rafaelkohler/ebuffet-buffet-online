package br.com.ebuffet.eBuffet.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_cliente")
	private Long idCliente;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_localidade")
	private Localidade localidade;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "documento")
	private String documento;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "celular")
	private String celular;
	
	@Column(name = "telefone")
	private String telefone;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "cliente_localidades", 
			joinColumns = { @JoinColumn(name = "id_cliente") }, 
			inverseJoinColumns = { @JoinColumn(name = "id_localidade") })
	private List<Localidade> localidades;
	
	@OneToMany(mappedBy = "cliente")
	@OrderBy("dataPedido DESC")
	private List<Pedido> pedidos;
	
	public Cliente() {
		
	}

	public Cliente(String nome, String documento, String email, String celular, String telefone,
			List<Localidade> localidades) {
		super();
		this.nome = nome;
		this.documento = documento;
		this.email = email;
		this.celular = celular;
		this.telefone = telefone;
		this.localidades = localidades;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (idCliente == null) {
			if (other.idCliente != null)
				return false;
		} else if (!idCliente.equals(other.idCliente))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCliente == null) ? 0 : idCliente.hashCode());
		return result;
	}
	
	

}
