package br.com.ebuffet.eBuffet.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "fretes")
public class Frete implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_frete")
	private Long idFrete;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name = "id_pedido")
	private Pedido pedido;
	
	@Column(name = "nome_empresa")
	private String nomeEmpresa;
	
	@Column(name = "cep_origem")
	private String cepOrigem;
	
	@Column(name = "cep_destino")
	private String cepDestino;
	
	@Column(name = "peso")
	private Double peso;

	@Column(name = "quantidade")
	private Integer quantidade;
	
	@Column(name = "aviso_recebimento")
	private String avisoRecebimento;
	
	@Column(name = "valor_declarado")
	private Double valorDeclarado;
	
	public Frete() {
		
	}

	public Frete(String nomeEmpresa, String cepOrigem, String cepDestino, Double peso, Integer quantidade,
			String avisoRecebimento, Double valorDeclarado) {
		super();
		this.nomeEmpresa = nomeEmpresa;
		this.cepOrigem = cepOrigem;
		this.cepDestino = cepDestino;
		this.peso = peso;
		this.quantidade = quantidade;
		this.avisoRecebimento = avisoRecebimento;
		this.valorDeclarado = valorDeclarado;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frete other = (Frete) obj;
		if (idFrete == null) {
			if (other.idFrete != null)
				return false;
		} else if (!idFrete.equals(other.idFrete))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idFrete == null) ? 0 : idFrete.hashCode());
		return result;
	}
	
	
}
