package br.com.ebuffet.eBuffet.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "localidades")
public class Localidade implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_localidade")
	private Long idLocalidade;
	
	@Column(name = "lat")
	private Double lat;
	
	@Column(name = "lng")
	private Double lng;
	
	@Column(name = "num")
	private Integer num;
	
	@Column(name = "bairro")
	private String bairro;

	@Column(name = "cep")
	private String cep;
	
	@Column(name = "cidade")
	private String cidade;
	
	@Column(name = "complemento")
	private String complemento;
	
	@Column(name = "rua")
	private String rua;

	@Column(name = "uf")
	private String uf;
	
	@OneToMany(mappedBy = "localidade")
	private List<Pedido> pedidos;

	public Localidade() {
		
	}
			
	public Localidade(Double lat, Double lng, Integer num, String bairro, String cep, String cidade, String complemento,
			String rua, String uf) {
		super();
		this.lat = lat;
		this.lng = lng;
		this.num = num;
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.complemento = complemento;
		this.rua = rua;
		this.uf = uf;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Localidade other = (Localidade) obj;
		if (idLocalidade == null) {
			if (other.idLocalidade != null)
				return false;
		} else if (!idLocalidade.equals(other.idLocalidade))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idLocalidade == null) ? 0 : idLocalidade.hashCode());
		return result;
	}
	
	
	
}
