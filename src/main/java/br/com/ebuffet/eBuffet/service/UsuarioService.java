package br.com.ebuffet.eBuffet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ebuffet.eBuffet.entity.Usuario;
import br.com.ebuffet.eBuffet.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository usuarioRep;
	
	
	public Usuario procurarUsuarioPorId(Long idUsuario) {
		return usuarioRep.findByIdUsuario(idUsuario);
	}

	public Usuario procurarUsuarioPorEmail(String email) {
		return usuarioRep.findByEmail(email);
	}
	
	public Usuario procurarUsuarioPorLogin(String login) {
		return usuarioRep.findByLogin(login);
	}

	public Usuario salvarUsuario(Usuario usuario) {
		return usuarioRep.save(usuario);
	}

	public List<Usuario> listarUsuario() {
		return usuarioRep.findAll();
	}; 

}
