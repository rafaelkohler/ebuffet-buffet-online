package br.com.ebuffet.eBuffet.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ebuffet.eBuffet.entity.Cliente;
import br.com.ebuffet.eBuffet.repository.ClienteRepository;
import br.com.ebuffet.eBuffet.service.exceptions.ObjectNotFoundException;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRep;
	
	
	public Cliente procurarClientePorId(Long idCLiente) {
		Optional<Cliente> cliente = clienteRep.findById(idCLiente);
		return cliente.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + idCLiente + ", Tipo: " + Cliente.class.getName()));
	}


	public Cliente salvarCliente(Cliente cliente) {
		return clienteRep.save(cliente);
	}; 

}
