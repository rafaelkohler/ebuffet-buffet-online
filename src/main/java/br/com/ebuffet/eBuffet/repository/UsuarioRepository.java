package br.com.ebuffet.eBuffet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ebuffet.eBuffet.entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Usuario findByIdUsuario(Long idUsuario);

	Usuario findByEmail(String email);

	Usuario findByLogin(String login);

}
